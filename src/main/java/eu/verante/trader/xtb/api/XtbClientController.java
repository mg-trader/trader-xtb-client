package eu.verante.trader.xtb.api;

import eu.verante.trader.stock.broker.BrokerResource;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.stock.symbol.TradingSymbol;
import eu.verante.trader.stock.symbol.TradingSymbolCategory;
import eu.verante.trader.xtb.service.XtbClientService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
public class XtbClientController implements BrokerResource {

    private final Log logger = LogFactory.getLog(XtbClientController.class);

    @Autowired
    private XtbClientService service;
    @Override
    public List<TradingSymbol> getTradingSymbols(
            String symbol, TradingSymbolCategory category, String groupName) {
        logger.info("Requesting for symbols: %s %s %s".formatted(symbol, category, groupName));
        return service.getTradingSymbolsBySymbolCategoryAndGroupName(symbol, category, groupName);
    }

    @Override
    public List<TradingCandle> getCandles(
            String symbol, Period period, long from) {
        ZonedDateTime fromConverted = ZonedDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.systemDefault());
        logger.info("Requesting for candles: %s %s %s".formatted(symbol, period, fromConverted));
        return service.getCandles(symbol, period, fromConverted);
    }
}
