package eu.verante.trader.xtb.value;

public enum SwapRolloverType implements ValuedEnum<Long> {
    MONDAY(0L),
    TUESDAY(1L),
    WEDNSDAY(2L),
    THURSDAY(3L),
    FRIDAY(4L);

    private final long value;

    SwapRolloverType(long value) {
        this.value = value;
    }

    @Override
    public Long getValue() {
        return value;
    }
}
