package eu.verante.trader.xtb.value;

public enum Period implements ValuedEnum<Integer> {
    MINUTE_1(1),
    MINUTE_5(5),
    MINUTE_15(15),
    MINUTE_30(30),
    HOUR_1(60),
    HOUR_4(240),
    DAY(1440),
    WEEK(10080),
    MONTH(4320);

    private final int value;

    Period(int value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public static Period fromDomainPeriod(eu.verante.trader.stock.candle.Period domainPeriod) {
        return Period.valueOf(domainPeriod.name());
    }
}
