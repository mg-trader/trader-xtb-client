package eu.verante.trader.xtb.value;

public enum SwapType implements ValuedEnum<Long> {

    POINTS(0L),
    DOLLARS(1L),
    INTEREST(2L),
    MARGIN_CURRENCY(3L);

    private final long value;

    SwapType(long value) {
        this.value = value;
    }

    @Override
    public Long getValue() {
        return value;
    }
}
