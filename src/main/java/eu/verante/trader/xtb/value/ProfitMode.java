package eu.verante.trader.xtb.value;

public enum ProfitMode implements ValuedEnum<Long> {

    FOREX(5L),
    CFD(6L);

    private final long value;

    ProfitMode(long value) {
        this.value = value;
    }


    @Override
    public Long getValue() {
        return value;
    }
}
