package eu.verante.trader.xtb.value;

import com.fasterxml.jackson.annotation.JsonValue;

public interface ValuedEnum<T> {
    @JsonValue
    T getValue();
}
