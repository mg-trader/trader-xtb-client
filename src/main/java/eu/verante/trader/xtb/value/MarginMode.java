package eu.verante.trader.xtb.value;

public enum MarginMode implements ValuedEnum<Long>{

    FOREX(101L),
    CFD_LEVERAGED(102L),
    CFD(103L),

    UNKNOWN(104L); //not documented

    private final long value;

    MarginMode(long value) {
        this.value = value;
    }

    @Override
    public Long getValue() {
        return value;
    }
}
