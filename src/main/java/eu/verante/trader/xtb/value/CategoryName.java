package eu.verante.trader.xtb.value;

import eu.verante.trader.stock.symbol.TradingSymbolCategory;

public enum CategoryName implements ValuedEnum<String>{
    STOCK("STC"),
    CRYPTO("CRT"),
    ETF("ETF"),
    INDEX("IND"),
    FOREX("FX"),
    STOCK_CLOSE_ONLY("STK"), //probably deprecated
    COMMODITY("CMD");
    private final String value;

    CategoryName(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }

    public static CategoryName fromDomainCategory(TradingSymbolCategory category) {
        return CategoryName.valueOf(category.name());
    }
}
