package eu.verante.trader.xtb.command;

public class GetChartLastRequestCommand
        extends AbstractXtbCommand<ArgumentsInfoWrapper<GetChartLastRequestArguments>>{

    private static final String GET_CHART_LAST_REQUEST = "getChartLastRequest";
    public GetChartLastRequestCommand(GetChartLastRequestArguments arguments) {
        super(GET_CHART_LAST_REQUEST, new ArgumentsInfoWrapper<>(arguments));
    }
}
