package eu.verante.trader.xtb.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
public abstract class AbstractXtbCommand<T extends XtbCommandArguments>
        implements XtbCommand<T> {

    private final String command;
    private final T arguments;
    private final String customTag = UUID.randomUUID().toString();

}
