package eu.verante.trader.xtb.command;

import eu.verante.trader.xtb.value.Period;
import lombok.Builder;

import java.time.ZonedDateTime;

@Builder
public record GetChartLastRequestArguments(Period period, ZonedDateTime start, String symbol)
        implements XtbCommandArguments {

}
