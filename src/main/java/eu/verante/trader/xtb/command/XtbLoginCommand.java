package eu.verante.trader.xtb.command;

public class XtbLoginCommand extends AbstractXtbCommand<XtbLoginArguments> {
    private static final String LOGIN_COMMAND = "login";
    public XtbLoginCommand(XtbLoginArguments arguments) {
        super(LOGIN_COMMAND, arguments);
    }
}
