package eu.verante.trader.xtb.command;

public interface XtbCommand<T extends XtbCommandArguments> {

    String getCommand();

    T getArguments();

    String getCustomTag();
}
