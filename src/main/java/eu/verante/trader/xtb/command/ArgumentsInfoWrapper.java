package eu.verante.trader.xtb.command;

public record ArgumentsInfoWrapper<T extends XtbCommandArguments>(T info)
        implements XtbCommandArguments{
}
