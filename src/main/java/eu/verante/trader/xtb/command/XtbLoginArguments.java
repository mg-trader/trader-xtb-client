package eu.verante.trader.xtb.command;

public record XtbLoginArguments(String userId, String password)
        implements XtbCommandArguments {

}
