package eu.verante.trader.xtb.command;

public class XtbGetAllSymbolsCommand  extends AbstractXtbCommand<VoidArguments> {

    private static final String GET_ALL_SYMBOLS_COMMAND = "getAllSymbols";
    public XtbGetAllSymbolsCommand() {
        super(GET_ALL_SYMBOLS_COMMAND, null);
    }
}
