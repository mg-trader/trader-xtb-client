package eu.verante.trader.xtb.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.xtb.command.*;
import eu.verante.trader.xtb.result.*;
import eu.verante.trader.xtb.value.Period;
import jakarta.websocket.ContainerProvider;
import jakarta.websocket.WebSocketContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.Future;

@Component
public class XtbConnectionAdapter {

    private static final int MAX_MESSAGE_SIZE = 20 * 1024 * 1024; //20MB
    private WebSocketSession webSocketSession;

    private String streamingSessionId;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private XtbSocketHandler socketHandler;

    @Autowired
    private XtbConnectionConfiguration connectionConfiguration;

    public synchronized List<XtbSymbol> getAllSymbols() {
        ensureLoggedIn();

        XtbGetAllSymbolsCommand command = new XtbGetAllSymbolsCommand();

        XtbGetAllSymbolsResult result = execute(command, XtbGetAllSymbolsResult.class);
        return result.getReturnData();
    }



    public synchronized XtbGetChartLastRequestResult getChartData(String symbol, ZonedDateTime fromDate, Period period) {
        ensureLoggedIn();

        GetChartLastRequestCommand command = new GetChartLastRequestCommand(
                new GetChartLastRequestArguments(period, fromDate, symbol)
        );

        return execute(command, XtbGetChartLastRequestResult.class);
    }

    private void ensureLoggedIn() {
        if (webSocketSession == null) {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.setDefaultMaxBinaryMessageBufferSize(MAX_MESSAGE_SIZE);
            container.setDefaultMaxTextMessageBufferSize(MAX_MESSAGE_SIZE);

            WebSocketClient simpleWebSocketClient =
                    new StandardWebSocketClient(container);


            socketHandler.initialize(this);

            try {
                webSocketSession = simpleWebSocketClient.execute(socketHandler, connectionConfiguration.getUrl()).get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            XtbLoginCommand command = new XtbLoginCommand(
                    new XtbLoginArguments(connectionConfiguration.getUserId(), connectionConfiguration.getPassword()));
            XtbLoginResult result = getResultSafe(sendCommand(command, XtbLoginResult.class));
            streamingSessionId = result.getStreamSessionId();
        }
    }

    private <T extends XtbResult> T execute(AbstractXtbCommand command, Class<T> expectedResultType) {
        return getResultSafe(sendCommand(command, expectedResultType));
    }

    private <T extends XtbResult> Future<T> sendCommand(XtbCommand command, Class<T> expectedResultType) {
        try {
            Future<T> result = socketHandler.subscribe(command.getCustomTag(), expectedResultType);
            TextMessage textMessage = new TextMessage(mapper.writeValueAsString(command));
            webSocketSession.sendMessage(textMessage);
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private <T extends XtbResult> T getResultSafe(Future<T> future) {
        try {
            return future.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected void invalidateSession() {
        this.webSocketSession = null;
    }
}
