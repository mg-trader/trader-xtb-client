package eu.verante.trader.xtb.socket;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "clients.xtb")
@Data
public class XtbConnectionConfiguration {
    private String url;
    private String userId;
    private String password;

}
