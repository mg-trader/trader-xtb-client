package eu.verante.trader.xtb.socket;

import eu.verante.trader.xtb.result.XtbErrorResult;

public class XtbRequestException  extends RuntimeException {

    private final XtbErrorResult result;

    public XtbRequestException(XtbErrorResult result) {
        super("Xtb request ended with error: " +result.getErrorCode()+" - " + result.getErrorDescr());
        this.result = result;
    }

    public XtbErrorResult getResult() {
        return result;
    }
}
