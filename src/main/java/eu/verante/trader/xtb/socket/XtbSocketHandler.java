package eu.verante.trader.xtb.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.xtb.result.XtbErrorResult;
import eu.verante.trader.xtb.result.XtbResult;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class XtbSocketHandler implements WebSocketHandler {

    private final Map<String, Pair<Class<?>, CompletableFuture<XtbResult>>> subscriptions = new HashMap<>();

    private final Log logger = LogFactory.getLog(XtbSocketHandler.class);
    @Autowired
    private ObjectMapper mapper;

    private XtbConnectionAdapter xtbAdapter;

    protected void initialize(XtbConnectionAdapter adapter) {
        this.xtbAdapter = adapter;
    }

    public<T extends XtbResult> Future<T> subscribe(String customTag, Class<T> expectedType) {
        CompletableFuture<XtbResult> result = new CompletableFuture<>();
        Pair<Class<?>, CompletableFuture<XtbResult>> pair = Pair.of(expectedType, result);
        subscriptions.put(customTag, pair);
        return (Future<T>) result;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        logger.info("Connection with XTB established");
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        if (message instanceof TextMessage) {
            handleTextMessage(session, (TextMessage) message);
        }
        else {
            throw new IllegalStateException("Unexpected WebSocket message type: " + message);
        }

    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
        logger.error("Transport error", exception);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) {
        logger.info("Connection closed: " + closeStatus.toString());
        xtbAdapter.invalidateSession();
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    private void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String payload = message.getPayload();
        String pattern = "\"customTag\":\"(?<tag>[0-9a-z-]*)\"";
        Pattern pattern1 = Pattern.compile(pattern);
        Matcher matcher = pattern1.matcher(payload);
        matcher.find();
        String tag = matcher.group("tag");

        Pair<Class<?>, CompletableFuture<XtbResult>> future = subscriptions.remove(tag);
        if (isErrorMessage(payload)) {
            XtbErrorResult xtbErrorResult = mapper.readValue(payload, XtbErrorResult.class);
            future.getRight().completeExceptionally(new XtbRequestException(xtbErrorResult));
        }
        XtbResult result = (XtbResult) mapper.readValue(payload, future.getLeft());
        future.getRight().complete(result);
    }

    private boolean isErrorMessage(String payload) {
        return payload.contains("\"errorCode\":");
    }
}
