package eu.verante.trader.xtb.result;

import lombok.Data;

@Data
public class XtbResult {
    private boolean status;

    private String customTag;
}
