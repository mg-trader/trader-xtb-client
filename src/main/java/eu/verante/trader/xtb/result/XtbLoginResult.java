package eu.verante.trader.xtb.result;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class XtbLoginResult extends XtbResult {
    private String streamSessionId;
}
