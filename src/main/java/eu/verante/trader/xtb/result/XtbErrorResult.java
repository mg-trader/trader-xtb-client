package eu.verante.trader.xtb.result;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class XtbErrorResult  extends XtbResult{

    private String errorCode;
    private String errorDescr;
}
