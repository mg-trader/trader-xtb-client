package eu.verante.trader.xtb.result;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class XtbChartRateInfo {

    private ZonedDateTime ctm;
    private double open;
    private double close;
    private double high;
    private double low;
    private double vol;

}
