package eu.verante.trader.xtb.result;

import eu.verante.trader.xtb.value.*;
import lombok.Data;

@Data
public class XtbSymbol {
    private double ask;
    private double bid;
    private String currency;
    private String currencyProfit;
    private String description;
    private int instantMaxVolume;
    private double high;
    private double low;
    private String symbol;
    private long time;
    private int type;
    private String groupName;
    private CategoryName categoryName;
    private boolean longOnly;
    private Long starting;
    private Long expiration;
    private int stepRuleId;
    private int stopsLevel;
    private double lotMax;
    private double lotMin;
    private double lotStep;
    private int precision;
    private Long contractSize;
    private Long initialMargin;
    private double marginHedged;
    private boolean marginHedgedStrong;
    private Long marginMaintenance;
    private MarginMode marginMode;
    private double percentage;
    private ProfitMode profitMode;
    private double spreadRaw;
    private double spreadTable;
    private boolean swapEnable;
    private double swapLong;
    private double swapShort;
    private SwapType swapType;
    private SwapRolloverType swapRollover;
    private double tickSize;
    private double tickValue;
    private int quoteId;
    private String timeString;
    private double leverage;
    private boolean currencyPair;
}
