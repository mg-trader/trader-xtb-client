package eu.verante.trader.xtb.result;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class XtbGetChartLastRequestResult extends XtbResult {

    private ReturnData returnData;

    @Data
    public static class ReturnData {
        private int digits;
        private List<XtbChartRateInfo> rateInfos;
    }
}
