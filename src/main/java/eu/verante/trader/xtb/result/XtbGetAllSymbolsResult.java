package eu.verante.trader.xtb.result;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class XtbGetAllSymbolsResult extends XtbResult {
    private List<XtbSymbol> returnData;

}
