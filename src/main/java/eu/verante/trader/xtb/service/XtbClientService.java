package eu.verante.trader.xtb.service;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.stock.symbol.TradingSymbol;
import eu.verante.trader.stock.symbol.TradingSymbolCategory;
import eu.verante.trader.xtb.result.XtbGetChartLastRequestResult;
import eu.verante.trader.xtb.result.XtbSymbol;
import eu.verante.trader.xtb.socket.XtbConnectionAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.verante.trader.xtb.value.CategoryName.STOCK_CLOSE_ONLY;
import static eu.verante.trader.xtb.value.CategoryName.fromDomainCategory;
import static eu.verante.trader.xtb.value.Period.fromDomainPeriod;

@Component
public class XtbClientService {

    @Autowired
    private XtbConnectionAdapter adapter;

    @Autowired
    private XtbClientMapper mapper;

    public List<TradingSymbol> getTradingSymbolsBySymbolCategoryAndGroupName(
            String symbol, TradingSymbolCategory category, String groupName) {
        return adapter.getAllSymbols().stream()
                .filter(s -> filterBySymbol(s, symbol))
                .filter(s -> filterByCategory(s, category))
                .filter(s -> filterByGroupName(s, groupName))
                .filter(s -> s.getCategoryName() != STOCK_CLOSE_ONLY) //do not use
                .map(mapper::mapToTradingSymbol)
                .collect(Collectors.toList());
    }

    private boolean filterBySymbol(XtbSymbol symbol, String symbolName) {
        return Optional.ofNullable(symbolName).map(sn -> symbol.getSymbol().equalsIgnoreCase(sn))
                .orElse(true);
    }

    private boolean filterByGroupName(XtbSymbol symbol, String groupName) {
        return Optional.ofNullable(groupName).map(gn -> symbol.getGroupName().equalsIgnoreCase(gn))
                .orElse(true);
    }

    private boolean filterByCategory(XtbSymbol symbol, TradingSymbolCategory category) {
        return Optional.ofNullable(category).map(c -> symbol.getCategoryName() == fromDomainCategory(c))
                .orElse(true);
    }

    public List<TradingCandle> getCandles(String symbol, Period period, ZonedDateTime from) {
        XtbGetChartLastRequestResult chartData = adapter.getChartData(symbol, from, fromDomainPeriod(period));
        return chartData.getReturnData().getRateInfos().stream()
                .map(data -> mapper.mapToCandle(data, symbol, period, chartData.getReturnData().getDigits()))
                .collect(Collectors.toList());
    }
}
