package eu.verante.trader.xtb.service;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.stock.symbol.TradingSymbol;
import eu.verante.trader.stock.symbol.TradingSymbolCategory;
import eu.verante.trader.xtb.result.XtbChartRateInfo;
import eu.verante.trader.xtb.result.XtbSymbol;
import eu.verante.trader.xtb.value.CategoryName;
import org.mapstruct.*;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface XtbClientMapper {

    @Mapping(source = "categoryName", target = "category")
    TradingSymbol mapToTradingSymbol(XtbSymbol xtbSymbol);

    @Mapping(source = "info.ctm", target = "time")
    @Mapping(source = "info.vol", target = "volume")
    @Mapping(target="open", expression="java(new BigDecimal(info.getOpen()).movePointLeft(digits))" )
    @Mapping(target="close", expression="java(new BigDecimal(info.getOpen() + info.getClose()).movePointLeft(digits))" )
    @Mapping(target="high", expression="java(new BigDecimal(info.getOpen() + info.getHigh()).movePointLeft(digits))" )
    @Mapping(target="low", expression="java(new BigDecimal(info.getOpen() + info.getLow()).movePointLeft(digits))" )
    @Mapping(source="symbol", target = "symbol")
    @Mapping(source="period", target = "period")
    TradingCandle mapToCandle(XtbChartRateInfo info, String symbol, Period period, int digits);

    @ValueMapping(source = "STOCK_CLOSE_ONLY", target = MappingConstants.NULL)
    TradingSymbolCategory mapToCategory(CategoryName category);
}
