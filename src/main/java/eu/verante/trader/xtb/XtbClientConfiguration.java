package eu.verante.trader.xtb;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.symbol.TradingSymbolCategory;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class XtbClientConfiguration implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(String.class, TradingSymbolCategory.class, StringToEnumConverter.forEnum(TradingSymbolCategory.class));
        registry.addConverter(String.class, Period.class, StringToEnumConverter.forEnum(Period.class));
    }
}
