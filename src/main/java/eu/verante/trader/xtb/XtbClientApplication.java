package eu.verante.trader.xtb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XtbClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(XtbClientApplication.class, args);
    }
}
