package eu.verante.trader.xtb;

import org.springframework.core.convert.converter.Converter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class StringToEnumConverter<E extends Enum<E>>
        implements Converter<String, E> {

    private final Map<String, E> mapping;

    private StringToEnumConverter(Class<E> targetClass) {
        Map<String, E> internalMapping = new HashMap<>();
        for (E enumConstant : targetClass.getEnumConstants()) {
            internalMapping.put(enumConstant.name().toLowerCase(), enumConstant);
        }
        mapping = Collections.unmodifiableMap(internalMapping);
    }

    public static <E extends Enum<E>> StringToEnumConverter<E> forEnum(Class<E> targetClass) {
        return new StringToEnumConverter<>(targetClass);
    }

    @Override
    public E convert(String source) {
        return mapping.get(source.toLowerCase());
    }
}
